 [![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/3sr-biblio/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/3sr-biblio/-/commits/master)

# Automated 3SR bibliography

Create [html pages](https://roubine.gricad-pages.univ-grenoble-alpes.fr/3sr-biblio/) for 3sr website bibliography
