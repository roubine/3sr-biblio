import datetime
import requests
import time

REQ_DOC_TYPE = "(docType_s:ART OR docType_s:OUV OR docType_s:COUV)"
BRIDGE_TYPES = {
    "ART": "Articles",
    "COMM": "Conferences",
    "COUV": "Books",
    "THESE": "Theses",
}
SORT_BY = "producedDate_s"  # "journalDate_s"
N_PUBLI_MAX = 100
N_PUBLIS = [3, 5, 10, 15, 50, 100]
MAX_AUTHORS = 3

STRUCTURES = [
    ("geo", [1043067, 1041793, 545340]),
    ("comhet", [1043065, 1041795, 545341]),
    ("rv", [1043066, 1041796, 545342]),
    ("labo", [1043064, 1041792, 706]),
]
ID_HALS = ["eroubin", "lucie-bailly", "sophiecapdevielle", "stefano-dal-pont", "laurent-daudeville", "alice-di-donna", "vieuxcfl"]


def api_call(req, timeout=60):
    """
    handles API calls and raise in case of errrors
    """

    r = requests.get(req, timeout=timeout)
    r.raise_for_status()

    try:
        j = r.json()
        return j

    except BaseException as e:
        raise TypeError(f"Deserialization impossible ({e})")


####################
# HELPER FUNCTIONS #
####################
def html_publi(publi):
    """
    formats a single publication data into an html line
    """
    cite = []

    # AUTHORS
    try:
        authors = []
        authors_title = []
        searchUrl = "https://hal.archives-ouvertes.fr/search/index/q/*"
        for fullname_idhal, name in zip(
            publi["authFullNameIdHal_fs"], publi["authLastName_s"]
        ):
            fullname, idhal = fullname_idhal.split("_FacetSep_")
            searchType = "authIdHal_s" if idhal else "authFullName_s"
            searchString = idhal if idhal else fullname.replace(" ", "+")
            url = f"{searchUrl}/{searchType}/{searchString}"
            authors.append(
                f'<span class="author"><a href="{url}" target="_blank">{name.title()}</a></span>'
            )
            authors_title.append(fullname)
    except Exception:
        authors = publi["authFullName_s"]
        authors_title = publi["authFullName_s"]

    # troncate if to many authors
    if len(authors) > MAX_AUTHORS:
        authors_displayed = authors[:MAX_AUTHORS]
        authors_displayed.append('<span class="author"><i>et. al.</i></span>')
    else:
        authors_displayed = authors
    cite.append(
        f'<span class="authors" title="{", ".join(publi.get("authFullName_s", []))}">[{", ".join(authors_displayed)}]</span>'
    )

    # TITLE
    if "uri_s" in publi and "title_s" in publi:
        cite.append(
            f'<span class="article"><a href="{publi["uri_s"]}" target="_blank">{publi["title_s"][0]}</a></span>'
        )

    # JOURNAL
    if publi.get("docType_s") == "ART":
        # get journal
        # fields = [("journalTitle_s", ""), ("volume_s", "vol."), ("page_s", "pp.")]
        # journal = [f'{v} {publi[k].title()}' for k, v in fields if k in publi]
        journal = [publi.get(k).title() for k in ["journalTitle_s"]]
        cite.append(f'<span class="journal">{", ".join(journal)}</span>')

    elif publi.get("docType_s") == "COUV":
        # get book
        journal = [publi.get(k).title() for k in ["bookTitle_s"]]
        cite.append(f'<span class="journal">{", ".join(journal)}</span>')

    elif publi.get("docType_s") == "COMM":
        # get conf
        journal = [publi.get(k) for k in ["conferenceTitle_s"]]
        cite.append(f'<span class="journal">{", ".join(journal)}</span>')

    elif publi.get("docType_s") == "THESE":
        # get conf
        journal = [
            ", ".join(publi.get(k, []))
            for k in ["labStructAcronym_s", "instStructAcronym_s"]
        ]
        cite.append(f'<span class="journal">{", ".join(journal)}</span>')

    # DATE
    if "producedDateY_i" in publi:
        cite.append(f'<span class="year">{publi["producedDateY_i"]}</span>')

    # DOI
    if "doiId_s" in publi:
        cite.append(
            f'<span class="doi"><a href="https://doi.org/{publi["doiId_s"]}" >{publi["doiId_s"]}</a></span>'
        )

    return " ".join(cite)


def get_biblio_structure(structure_name, structure_id, n_publi=100):
    """
    calls the HAL API for a structure and create the HTML file
    """
    structures = " OR ".join([str(_) for _ in structure_id])
    # req = f'https://api.archives-ouvertes.fr/search/?q=({REQ_DOC_TYPE} AND authStructId_i:({structures}))&sort={SORT_BY} desc&rows={N_PUBLI_MAX}&fl=*'
    req = f"https://api.archives-ouvertes.fr/search/?q=({REQ_DOC_TYPE} AND structId_i:({structures}))&sort={SORT_BY} desc&rows={N_PUBLI_MAX}&fl=*"
    tic = time.time()
    publis = api_call(req)
    toc = time.time()
    if publis["response"]["numFound"] == 0:
        raise (Exception(f"No publi found (struct = {structures})"))

    print(f'{structure_name:<10} -> {publis["response"]["numFound"]: 4d} publi found ({toc - tic:.2f}s)')

    def write_from_publi(publis, n):
        file = f"{structure_name}-{n}.html"
        with open(file, "w") as f:
            f.write("<!DOCTYPE html>\n")
            f.write("<html>\n")
            f.write("\t<head>\n")
            f.write('\t\t<meta charset="UTF-8">\n')
            f.write('\t\t<link rel="stylesheet" href="biblio.css">\n')
            f.write("\t</head>\n")
            f.write('\t<body class="structure">\n')
            f.write("\t\t<ul>\n")
            for publi in publis["response"]["docs"][:n]:
                f.write(f"\t\t\t<li>{html_publi(publi)}</li>\n")
            f.write("\t\t</ul>\n")
            f.write(f'\t\t<p class="update">Sources: <a href="{req}">HAL</a> - {datetime.datetime.today():%d/%m/%y %H:%M:%S}</p>\n')
            f.write("\t</body>\n")
            f.write("</html>")
        return file

    files = []
    for n in N_PUBLIS:
        files.append(write_from_publi(publis, n))

    return files


def get_biblio_idhal(idhal):
    req = f"https://api.archives-ouvertes.fr/search/?q=(authIdHal_s:{idhal})&sort={SORT_BY} desc&rows={N_PUBLI_MAX}&fl=*"
    tic = time.time()
    publis = api_call(req)
    toc = time.time()

    print(f'{idhal:<10} -> {publis["response"]["numFound"]: 4d} publi found ({toc - tic:.2f}s)')

    if publis["response"]["numFound"] == 0:
        raise (Exception(f"No publi found (idhal = {idhal})"))

    # req_author = f"https://api.archives-ouvertes.fr/ref/author/?q=idHal_s:{idhal}"
    # idhal_info = api_call(req_author)

    publis_by_type = {"Articles": [], "Books": [], "Conferences": [], "These": []}
    publis = publis["response"]["docs"]
    for i, publi in enumerate(publis):
        type = (
            BRIDGE_TYPES.get(publi.get("docType_s"))
            if BRIDGE_TYPES.get(publi.get("docType_s")) is not None
            else publi.get("docType_s")
        )
        if type in publis_by_type:
            publis_by_type[type].append(publi)
        else:
            publis_by_type[type] = [publi]

    file = f"{idhal}.html"
    with open(file, "w") as f:
        f.write("<!DOCTYPE html>\n")
        f.write("<html>\n")
        f.write("\t<head>\n")
        f.write('\t\t<meta charset="UTF-8">\n')
        f.write('\t\t<link rel="stylesheet" href="biblio.css">\n')
        f.write("\t</head>\n")
        f.write('\t<body class="perso">\n')
        for type, publis in publis_by_type.items():
            if not len(publis):
                continue
            f.write(f"\t\t<h4>{type} ({len(publis)})</h4>\n")
            f.write("\t\t<ul>\n")
            for publi in publis:
                f.write(f"\t\t\t<li>{html_publi(publi)}</li>\n")
            f.write("\t\t</ul>\n")
        f.write(
            f'\t\t<p class="update">Sources: <a href="{req}">HAL</a> - {datetime.datetime.today():%d/%m/%y %H:%M:%S}</p>\n'
        )
        f.write("\t</body>\n")
        f.write("</html>")

    return [file]


#########################
# GET DATA FROM HAL API #
#########################
all_files = {}

# Loop over structures
for struct, ids in STRUCTURES:
    try:
        if struct not in all_files:
            all_files[struct] = []
        all_files[struct] += get_biblio_structure(struct, ids)
    except ConnectionError as e:
        print(f'{struct:<10} -> Connection Error ({e})')
    except requests.HTTPError as e:
        print(f'{struct:<10} -> HTTP Error ({e})')
    except requests.Timeout as e:
        print(f'{struct:<10} -> Timeout ({e})')
    except requests.exceptions.RequestException as e:
        print(f'{struct:<10} -> Request Exception ({e})')
    except BaseException as e:
        print(f'{struct:<10} -> Error ({e})')

# Loop over idhals
for idhal in ID_HALS:
    try:
        if "idhal" not in all_files:
            all_files["idhal"] = []
        all_files["idhal"] += get_biblio_idhal(idhal)
    except ConnectionError as e:
        print(f'{idhal:<10} -> Connection Error ({e})')
    except requests.HTTPError as e:
        print(f'{idhal:<10} -> HTTP Error ({e})')
    except requests.Timeout as e:
        print(f'{idhal:<10} -> Timeout ({e})')
    except requests.exceptions.RequestException as e:
        print(f'{idhal:<10} -> Request Exception ({e})')
    except BaseException as e:
        print(f'{idhal:<10} -> Error ({e})')

############################
# WRITE DATA IN HTML FILES #
############################
with open("index.html", "w") as f:
    f.write("<html><body>")
    for type, files in all_files.items():
        f.write(f"<h1>{type}</h1>")
        f.write("<ul>")
        for file in files:
            f.write(f'<li><a href="{file}">{file}</a></li>')
        f.write("</ul>")
    f.write("</body></html>")
